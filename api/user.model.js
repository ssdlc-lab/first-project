import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
    Name: {
      type: String,
      required: true,
    },
    Age: {
        type: Number,
        required: true,
    }
  });

  const User = mongoose.model('User', userSchema);

  export default User;