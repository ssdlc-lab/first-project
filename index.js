import express from "express";
import routes from './api/user.routes.js';
import dbConnect from "./config/db.js";

dbConnect();

const App = express();
App.use(express.json());

App.get("/",(req,res) => {
    res.send("Hello World");

});

// Use user routes
App.use('/api', routes);

const port = 5000;

App.listen(port, () => {
    console.log(`server is running on port: ${port}`);
});